package OXGame;


public class CheckGame {
	 int endgame;
	 int row;
	 int col;
	 int count;
	 int turn;
	 int r;
	 int c;
         Table tableXO =  new Table();
         ShowResult print = new ShowResult();
         boolean ret = false;
         
	public  boolean checkError() {
		if ((col > 2 || col < 0) && (row > 2 || row < 0)) {
			print.showErrorRC();
		} else if (row > 2 || row < 0) {
			print.showErrorRow();
		} else if (col > 2 || col < 0) {
			print.showErrorCol();
		} else if (Table.tableOX[row][col]=='O'|| Table.tableOX[row][col]=='X') {
			print.showErrorChosen();
		ret = false;
                }
		return ret;
	}

	public  boolean checkTie() {
		if (count == 9 && endgame == 0) {
			tableXO.showTable();
			print.showTie();
			endgame++;
		ret = true;
                }
                return ret;
	}
        public  boolean checkOWinRow() {
		if ((Table.tableOX[row][col] == 'O' && Table.tableOX[row][col+1] == 'O' && Table.tableOX[row][col+2] == 'O')
				|| (Table.tableOX[row+1][col] == 'O' && Table.tableOX[row+1][col+1] == 'O' && Table.tableOX[row+1][col+2] == 'O')
				|| (Table.tableOX[row+2][col] == 'O' && Table.tableOX[row+2][col+1] == 'O' && Table.tableOX[row+2][col+2] == 'O')) {
			tableXO.showTable();
			print.showOWin();
			endgame++;
		ret = true;
                }
                return ret;
	}
        public  boolean checkOWinCol() {
		if ((Table.tableOX[row][col] == 'O' && Table.tableOX[row+1][col] == 'O' && Table.tableOX[row+2][col] == 'O')
				|| (Table.tableOX[row][col+1] == 'O' && Table.tableOX[row+1][col+1] == 'O' &&Table.tableOX[row+2][col+1] == 'O')
				|| (Table.tableOX[row][col+2] == 'O' && Table.tableOX[row+1][col+2] == 'O' && Table.tableOX[row+2][col+2] == 'O')) {
			tableXO.showTable();
			print.showOWin();
			endgame++;
		ret = true;
                }
                return ret;
	}
        public  boolean checkOWinX() {
		if ((Table.tableOX[0][0] == 'O' && Table.tableOX[1][1] == 'O' && Table.tableOX[2][2] == 'O')
			|| (Table.tableOX[0][2] == 'O' && Table.tableOX[1][1] == 'O' && Table.tableOX[2][0] == 'O')) {
			tableXO.showTable();
			print.showOWin();
			endgame++;
		ret = true;
                }
                return ret;
	}
        public  boolean checkXWinRow() {
		if ((Table.tableOX[row][col] == 'X' && Table.tableOX[row][col+1] == 'X' && Table.tableOX[row][col+2] == 'X')
				|| (Table.tableOX[row+1][col] == 'X' && Table.tableOX[row+1][col+1] == 'X' &&Table.tableOX[row+1][col+2] == 'X')
				|| (Table.tableOX[row+2][col] == 'X' && Table.tableOX[row+2][col+1] == 'X' && Table.tableOX[row+2][col+2] == 'X')) {
			tableXO.showTable();
			print.showXWin();
			endgame++;
		ret = true;
                }
                return ret;
	}
        public  boolean checkXWinCol() {
		if ((Table.tableOX[row][col] == 'X' && Table.tableOX[row+1][col] == 'X' && Table.tableOX[row+2][col] == 'X')
				|| (Table.tableOX[row][col+1] == 'X' && Table.tableOX[row+1][col+1] == 'X' &&Table.tableOX[row+2][col+1] == 'X')
				|| (Table.tableOX[row][col+2] == 'X' && Table.tableOX[row+1][col+2] == 'X' && Table.tableOX[row+2][col+2] == 'X')) {
			tableXO.showTable();
			print.showXWin();
			endgame++;
		ret = true;
                }
                return ret;
	}
        public  boolean checkXWinX() {
		if ((Table.tableOX[0][0] == 'X' && Table.tableOX[1][1] == 'X' && Table.tableOX[2][2] == 'X')
			|| (Table.tableOX[0][2] == 'X' && Table.tableOX[1][1] == 'X' && Table.tableOX[2][0] == 'X')) {
			tableXO.showTable();
			print.showOWin();
			endgame++;
		ret = true;
                }
                return ret;
	}

	

}
