package OXGame;


import java.util.Scanner;

public class OXOOP {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Table tableXO = new Table();
        ShowResult print = new ShowResult();
        int endgame = 0;
        int row = 0;
        int col = 0;
        int count = 0;
        int turn = 0;
        int owin = 0;
        int xwin = 0;
        char ans = 'Y';
        print.showWelcome();
        CheckGame check = new CheckGame();
        TurnO O= new TurnO();
        TurnX X= new TurnX();
        while (true) {
            System.out.println();
            if (ans == 'N') {
                if (owin > xwin) {
                    System.out.println("Finally, Player O is winner!!");
                } else if (xwin > owin) {
                    System.out.println("Finally, Player X is winner!!");
                } else {
                    System.out.println("Finally, It's a tie!!");
                }
                System.out.println("Let's play again next time!!");
                break;
            } else if (ans == 'Y') {
                while (endgame == 0) {
                    tableXO.showTable();
                    
                    if ((owin == xwin && owin%2==0) || owin > xwin) {
                        O.TurnO();
                        count++;
                        if (check.checkTie() ==true) {
                            endgame++;
                        } else if (check.checkOWinRow() ==true
                                || check.checkOWinCol() ==true
                                || check.checkOWinX() ==true
                                && endgame == 0) {
                            endgame++;
                            owin++;
                        } 
                        tableXO.showTable();
                        if (endgame == 0) {
                            X.TurnX();
                            count++;
                            if (check.checkXWinRow() ==true
                                    || check.checkXWinCol() ==true
                                    || check.checkXWinX() ==true
                                    && endgame == 0) {
                                endgame++;
                                xwin++;
                            } else if (check.checkTie()==true) {
                                endgame++;
                            }
                        }
                    } else {
                        X.TurnX();
                        count++;
                        if (check.checkTie() ==true) {
                            endgame++;
                        } else if (check.checkXWinRow()==true
                                || check.checkXWinCol()==true
                                || check.checkXWinX()==true && endgame == 0) {
                            endgame++;
                            xwin++;
                        }
                        else if (endgame == 0) {
                            tableXO.showTable(); 
                            O.TurnO();
                            count++;
                            if (check.checkOWinRow() ==true
                                    || check.checkOWinCol() ==true
                                    || check.checkOWinX()==true&& endgame == 0) {
                                endgame++;
                                owin++;
                            } else if (check.checkTie() ==true) {
                                endgame++;
                            }
                        }
                    }

                    if (endgame != 0) {
                        System.out.println("\n-------- Score --------");
                        System.out.println("O's score : " + owin);
                        System.out.println("X's score : " + xwin);
                        System.out.println("\nGame is over. Do you want to play again?");
                        System.out.print("Please input Y/N : ");
                        ans = kb.next().charAt(0);
                        if (ans == 'Y') {
                            endgame = 0;
                            count = 0;
                            check.ret = false;
                            System.out.println("\n-------- One More!! --------");
                            tableXO.newTable();
                        }
                    }
                }

            } else {
                if (endgame != 0) {
                    System.out.print("Please input Only Y or N : ");
                    ans = kb.next().charAt(0);
                }
            }

        }

    }
}
