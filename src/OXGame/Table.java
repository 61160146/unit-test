package OXGame;

public class Table {

    static char[][] tableOX = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static int row;
    static int col;
    static char XO;
    static char X;
    static char O;
    boolean check = false;

    public void setX(int row, int col) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tableOX[row][col] = 'X';
            }
        }
    }

    public void setO(int row, int col) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tableOX[row][col] = 'O';
            }
        }
    }

    public void setTableOX(char[][] tableOX) {
        Table.tableOX = tableOX;
    }

    public void showTable() {
        for (int i = 0; i < 3; i++) {
            System.out.println("+---+---+---+");
            for (int j = 0; j < 3; j++) {
                System.out.print("| ");
                System.out.print(tableOX[i][j]);
                System.out.print(" ");
            }
            System.out.print("| ");
            System.out.println();
        }
        System.out.println("+---+---+---+");
    }

    public void newTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tableOX[i][j] = '-';
            }
        }

    }

}
